.. _device-reference:

Device reference
================

This document describes the available devices on the startup.

.. toctree::
    :maxdepth: 1

    devices/computercraft/command
    devices/computercraft/computer
    devices/computercraft/drive
    devices/computercraft/harddrive
    devices/computercraft/http
    devices/computercraft/identification
    devices/computercraft/keyboard
    devices/computercraft/modem
    devices/computercraft/monitor
    devices/computercraft/printer
    devices/computercraft/redstone
    devices/computercraft/settings
    devices/computercraft/speaker
    devices/computercraft/terminal
    devices/opencomputers/keyboard
