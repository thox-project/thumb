.. _thumb:

thumb, a unified bootloader for in-game Lua environments
========================================================

thumb is a unified bootloader for `ComputerCraft: Tweaked`_,
OpenComputers_ and other relevant Lua 5.x environments.

For more information, consult the following references:

* `The repository <https://gitlab.com/thox-project/thumb>`_.
* `The issues tracker <https://gitlab.com/thox-project/thumb/-/issues>`_,
  in case you're encountering problems or want to suggest what thumb should
  add next.

This documentation is organized using `Diátaxis`_' structure.

Any question? Any issue? Any security flaw, such as a sandbox escape?
You can either contact me by mail at thomas@touhey.fr or on the
`Computer Mods Discord Server`_ (``@cake``).

.. warning::

    This project is in early alpha and documentation-driven, which means the
    documentation will very probably be in advance on the code. This is because
    I strongly believe that **while the code might disappear, what will stay
    is the concepts and the user experience** attempted at in this project,
    and the notions covered in the documentation (including protocols' and
    file formats descriptions).

.. toctree::
    :maxdepth: 2

    topics
    buses
    devices
    api

.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _OpenComputers: https://ocdoc.cil.li/
.. _Diátaxis: https://diataxis.fr/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
