Discussion topics
=================

In this section, we will explore discussion topics regarding the project's
environment and design. These topics provide keys to understanding what is
happening in this project, and why it is happening.

.. toctree::
    :maxdepth: 1

    topics/concepts
    topics/computercraft
    topics/opencomputers
