API Reference
=============

This document describes the API accessible by the bootstrapped environment.

.. toctree::
    :maxdepth: 2

    api/standard
    api/abstract
