``computercraft.side`` -- ComputerCraft side bus
================================================

This represents the peripherals placed directly next to a ComputerCraft
computer.

Valid addresses on this bus are ``"bottom"``, ``"top"``, ``"left"``,
``"right"``, ``"front"`` and ``"back"``.
