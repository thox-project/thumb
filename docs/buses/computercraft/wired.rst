``computercraft.wired`` -- ComputerCraft wired bus
==================================================

This represents devices connected to a ComputerCraft computer through
a wired modem.
