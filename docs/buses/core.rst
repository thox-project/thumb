``core`` -- Core bus
====================

This bus represents the virtual devices directly accessible to the computer
through APIs, such as the `redstone API`_ in ComputerCraft.

.. _redstone API: https://tweaked.cc/module/redstone.html
