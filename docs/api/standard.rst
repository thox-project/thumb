Standard APIs
=============

.. todo::

    Should we put into place a complete cross-version environment,
    or instead setup the appropriate environment for the current Lua version?

The following utilities are available as described in the `Lua 5.3 manual`_:

::

    _G
    _VERSION

    assert
    error
    getmetatable
    ipairs
    load
    next
    pairs
    pcall
    rawequal
    rawget
    rawlen
    rawset
    select
    setmetatable
    tonumber
    tostring
    type
    xpcall

    coroutine.create
    coroutine.resume
    coroutine.running
    coroutine.status
    coroutine.wrap
    coroutine.yield

    math.abs
    math.acos
    math.asin
    math.atan
    math.ceil
    math.cos
    math.deg
    math.exp
    math.floor
    math.fmod
    math.huge
    math.log
    math.max
    math.min
    math.modf
    math.pi
    math.rad
    math.random
    math.randomseed
    math.sin
    math.sqrt
    math.tan

    os.clock
    os.date
    os.difftime
    os.time

    string.byte
    string.char
    string.dump
    string.find
    string.format
    string.gmatch
    string.gsub
    string.len
    string.lower
    string.match
    string.rep
    string.reverse
    string.sub
    string.upper

    table.concat
    table.insert
    table.pack
    table.remove
    table.sort
    table.unpack

On certain conditions, the debug utilities might be available:

::

    debug.gethook
    debug.getinfo
    debug.getlocal
    debug.getmetatable
    debug.getupvalue
    debug.getuservalue
    debug.sethook
    debug.setlocal
    debug.setmetatable
    debug.setupvalue
    debug.setuservalue
    debug.traceback
    debug.upvalueid
    debug.upvaluejoin

On Lua 5.3 if available, the following utilities are also available:

::

    coroutine.isyieldable

    string.pack
    string.unpack
    string.packsize

    table.move

    math.maxinteger
    math.mininteger
    math.tointeger
    math.type
    math.ult

    utf8.char
    utf8.charpattern
    utf8.codes
    utf8.codepoint
    utf8.len
    utf8.offset

.. todo::

    Check if we have access to debug, in what circumstances, and so on.

.. _Lua 5.3 manual: https://www.lua.org/manual/5.3/manual.html
