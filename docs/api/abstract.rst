Startup abstraction layer
=========================

Above the standard layer, **only two functions are added**: :lua:func:`pull`,
and :lua:func:`call`.

No other elements will be added, so in order to provide a sandbox in a
minimal way, you can simply remove these two functions.

The abstraction layer is event-based; all information is provided through
events initially piled up, that help building the picture of what the
computer's physical system resembles.

Event interfaces
----------------

All events are provided as tables with no additional metadata.
These classes represent the properties present in such tables, and their
types and roles.

.. lua:class:: Event

    An event on the component abstraction, as returned by :lua:func:`pull`.
    Note that attributes include, but **are not limited** to the list
    provided below.

    .. lua:attribute:: type: string

        Type of the event, to be used as a discriminator.

.. lua:class:: BusEvent: Event

    Event raised when an event occurs on a bus, that does not concern
    a device directly.

    .. lua:attribute:: id: number

        Numeric identifier of the bus to which the event applies.

.. lua:class:: DeviceEvent: Event

    Event raised when an event occurs on a device.

    .. lua:attribute:: id: number

        Numeric identifier of the component to which the event applies.

Bus events
----------

.. lua:class:: BusAttachEvent: BusEvent

    Event raised when a new bus is attached.

    .. lua:attribute:: type: string

        Set to ``"bus_attach"``.

    .. lua:attribute:: bus: number | nil

        Identifier of the bus to which the bus is attached.

    .. lua:attribute:: bus_type: string

        Type of the bus, as documented in :ref:`bus-reference`.

.. lua:class:: BusDetachEvent: BusEvent

    Event raised when a bus is detached.

    This event can only be raised if **no device nor bus is still attached to
    the bus**. If this happens, it is an incorrect behaviour from the
    startup, and should be reported and fixed.

    .. lua:attribute:: type: string

        Set to ``"bus_detach"``.

Device events
-------------

.. lua:class:: DeviceAttachEvent: DeviceEvent

    Event raised when a device is attached.

    .. lua:attribute:: type: string

        Set to ``"device_attach"``.

    .. lua:attribute:: bus: number

        Numeric identifier of the bus to which the device has been
        attached to.

    .. lua:attribute:: address: string

        The address the device was attached on the bus.

    .. lua:attribute:: device_type: string

        Type of the device, as documented in :ref:`device-reference`.

.. lua:class:: DeviceDetachEvent: DeviceEvent

    Event raised when a device is detached.

    .. lua:attribute:: type: string

        Set to ``"device_detach"``.

Operations
----------

.. lua:function:: pull(timeout_sec)

    Pull the next event out of the queue, while waiting for ``timeout_sec``
    seconds at a maximum.

    .. note::

        Fractional seconds are allowed and will be fulfilled within the
        underlying platform's capabilities.

    If ``timeout_sec`` is lesser or equal to 0, the function will return
    immediately with an event if available, or with ``nil`` otherwise.

    .. note::

        This function can also be used to sleep, in a power-saving effort.
        Therefore, if all your processes are either awaiting an external
        event or other processes, it is recommended to call this method
        with long timeouts.

    If a timeout has occurred, the returned event will be ``nil``.

    :param timeout_sec: Timeout in seconds.
    :type timeout_sec: number
    :return: The next event in the queue, or ``nil``.
    :rtype: Event

.. lua:function:: call(id, name, ...)

    Call the method identified by its name ``name`` on the component
    which the identifier is ``id`` with the given arguments.

    :param id: Identifier of the component of which to call the method.
    :type id: number
    :param name: The name of the method to call.
    :type name: string
    :return: The raw return value of the method, or ``nil`` if the method
        doesn't exist on the given peripheral.
