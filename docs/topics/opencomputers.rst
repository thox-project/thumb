startup implementation details for OpenComputers and derivatives
================================================================

In the future, the ``startup-oc`` package will provide the startup environment
for OpenComputers and derivatives.

.. todo::

    The primitive for such a system is the components, with signals as
    events.

See `<https://ocdoc.cil.li/component:signals>`_ for signals.
