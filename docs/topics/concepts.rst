startup concepts
================

The startup structure is designed for being lightweight and easier to implement
on the platforms thox is designed for.

The environment provided by the startup script is composed of two main
components:

* A standard Lua environment, suited for the current Lua version.
* An event-based component cross-platform abstraction.

The startup script boots the system in a component-less state, where no
components seem loaded. From there, the system has access to two main
functions:

* :lua:func:`startup.pull` for sleeping and pulling events.
* :lua:func:`startup.call` for calling components in a synchronous fashion.

The initial few pulls get attach events for abstract and cold-plug devices,
mostly the core bus (see :ref:`through:bus-core`) which is usually (but not
guaranteed to be) device number 0 and its devices including hot-plug buses
such as the side bus on ComputerCraft (see
:ref:`through:bus-computercraft-side`) and the universal bus on OpenComputers
(see :ref:`through:bus-opencomputers`).

It is required for systems started up using these sets of scripts to pull
events regularly; if the number of awaiting events exceeds an
implementation-specific limit (depending on available memory and eventual DOS
countermeasures such as a watchdog), the system might crash.

For power-saving purposes, it is recommended for the system to sleep as much
as possible; the startup API design allows this while still being interrupted
as soon as an event occurs, within the limits of the current platform's
capabilities.
