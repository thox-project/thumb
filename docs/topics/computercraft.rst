startup implementation details for ComputerCraft and derivatives
================================================================

The ``startup-cc`` package provides the startup environment for ComputerCraft,
ComputerCraft:Tweaked and CC:T emulators such as `CCEmuX`_ and `CraftOS-PC`_.

.. todo::

    The primitive for such a system is the peripherals, with the side
    (see :ref:`through:bus-computercraft-side`) and wired
    (:ref:`through:bus-computercraft-wired`) buses, plus the native APIs
    abstracting core capabilities that are not part of the code execution and
    are abstracted away in a component.

.. todo::

    Describe the steps up to this point: the Java functions, how they are
    provided to Lua, the `bios.lua`_, then how and when the current startup
    script is loaded.

    Maybe quote the `unbios.lua`_ script which could be useful.

.. _CCEmuX: https://emux.cc/
.. _CraftOS-PC: https://www.craftos-pc.cc/
.. _Issue #710: ttps://github.com/cc-tweaked/CC-Tweaked/issues/710
.. _bios.lua: ttps://github.com/cc-tweaked/CC-Tweaked/blob/b97e950d86e4694409c01e507db34a87da85e452/src/main/resources/data/computercraft/lua/bios.lua
.. _unbios.lua: https://gist.github.com/MCJack123/42bc69d3757226c966da752df80437dc
