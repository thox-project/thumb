# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.

SPHINXOPTS    =
SPHINXBUILD   = poetry run sphinx-build
SPHINXWATCH   = poetry run sphinx-autobuild
SOURCEDIR     = .
BUILDDIR      = _build
LINKCHECKDIR  = _build/linkcheck
WEBROOT       = hercule:thox

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

requirements.txt:
	pip-compile --generate-hashes --no-emit-index-url \
		--output-file=requirements.txt pyproject.toml

.PHONY: prepare

# Livehtml build.
livehtml:
	$(SPHINXWATCH) -b html $(SPHINXOPTS) . $(BUILDDIR)/html \
		--ignore "**/.*.kate-swp" --watch _static

.PHONY: livehtml

# Check links.
checklinks:
	$(SPHINXBUILD) -b linkcheck $(ALLSPHINXOPTS) $(SOURCEDIR) $(LINKCHECKDIR)
	@echo "Check finished. Report is in $(LINKCHECKDIR)."

# Send the website content (Linux-only).
show: clean html
	find _build/html -type f -exec chmod 644 {} \;
	rsync -Prlt --exclude 'google*.html' --delete _build/html/ "$(WEBROOT)"

.PHONY: show
