``opencomputers.keyboard`` -- OpenComputers keyboard
====================================================

.. lua:module:: opencomputers.keyboard

This device represents a keyboard_, which raises the ``key_down``,
``key_up`` and ``clipboard`` `OpenComputers signals`_.

Available methods
-----------------

*This device does not make methods available.*

Events
------

.. lua:class:: KeyDownEvent: DeviceEvent

    This event is raised when a key is pressed on the keyboard.

    .. lua:attribute:: type: string

        Set to ``"key_down"``.

    .. lua:attribute:: code: number

        Numeric value of the pressed key.

    .. lua:attribute:: player_name: string

        Name of the player who has triggered the event.

.. lua:class:: KeyUpEvent: DeviceEvent

    This event is raised when a key is released on the keyboard.

    .. lua:attribute:: type: string

        Set to ``"key_up"``.

    .. lua:attribute:: code: number

        Numeric value of the released key.

    .. lua:attribute:: player_name: string

        Name of the player who has triggered the event.

.. lua:class:: ClipboardEvent: DeviceEvent

    This event is raised when a value has been pasted to the physical
    keyboard out of the game (breaking the fourth wall).

    .. lua:attribute:: type: string

        Set to ``"clipboard"``.

    .. lua:attribute:: content: string

        Content that was pasted via the keyboard.

    .. lua:attribute:: player_name: string

        Name of the player who has triggered the event.

.. _Keyboard: https://ocdoc.cil.li/block:keyboard
.. _OpenComputers signals: https://ocdoc.cil.li/component:signals
