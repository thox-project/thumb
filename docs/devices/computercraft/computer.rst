``computercraft.computer`` -- ComputerCraft computer
====================================================

.. lua:module:: computercraft.computer

This device represents a `computer peripheral`_.

Available methods
-----------------

.. lua:function:: turnOn()

    Turn the device on.

.. lua:function:: shutdown()

    Turn the device off.

.. lua:function:: reboot()

    Reboot the device.

.. lua:function:: getID()

    Get the device's computer identifier.

    :return: The device's identifier.
    :rtype: number

.. lua:function:: isOn()

    Get whether the device is on or not.

    :return: Whether the device is on or not.
    :rtype: boolean

.. lua:function:: getLabel()

    Get the device's label.

    :return: The device's label.
    :rtype: string | nil

Events
------

*This device does not raise events.*

.. _computer peripheral: https://tweaked.cc/peripheral/computer.html
