``computercraft.keyboard`` -- ComputerCraft keyboard
====================================================

.. lua:module:: computercraft.keyboard

This device is an abstract device provided by thumb, which makes the
`key <key event_>`_ and `keyup <keyup event_>`_ events accessible.

Available methods
-----------------

*This device does not make methods available.*

Events
------

.. note::

    The original `key event`_ has a third argument representing whether
    the event was raised while holding the key or pressing it for the
    first time, i.e. repeating key support. This abstraction eliminates
    this by filtering repeat events out and disabling support for this
    feature, in favour of managing it internally.

.. lua:class:: KeyDownEvent: DeviceEvent

    Event raised when a key is pressed on the keyboard.

    .. lua:attribute:: type: string

        Set to ``"key_down"``.

    .. lua:attribute:: code: number

        Numeric value of the pressed key.

.. lua:class:: KeyUpEvent: DeviceEvent

    Event raised when a key is released on the keyboard.

    .. lua:attribute:: type: string

        Set to ``"key_up"``.

    .. lua:attribute:: code: number

        Numeric value of the released key.

.. _key event: https://tweaked.cc/event/key.html
.. _keyup event: https://tweaked.cc/event/key_up.html
