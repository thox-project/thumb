``computercraft.command`` -- ComputerCraft server command peripheral
====================================================================

.. lua:module:: computercraft.command

This device represents a `command peripheral`_.

Available methods
-----------------

.. lua:function:: getCommand()

    Get the command currently stored on the device.

    :return: The current command.
    :rtype: string

.. lua:function:: setCommand(command)

    Replace the command currently stored on the device by the one provided.

    :param command: Command to store in the command module.
    :type command: string

.. lua:function:: runCommand()

    Run the command currently stored on the device.

    :return: A boolean if the command completed successfully, or a failure
        message or ``nil`` otherwise.
    :rtype: boolean | string | nil

Events
------

*This device does not raise events.*

.. _command peripheral: https://tweaked.cc/peripheral/command.html
