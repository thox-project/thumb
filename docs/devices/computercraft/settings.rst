``computercraft.settings`` -- ComputerCraft settings module
===========================================================

.. lua:module:: computercraft.settings

This device is a representation of the `settings module`_ as an external
device.

Available methods
-----------------

.. lua:function:: set(name, value)

    Set the value of a setting.

    :param name: Name of the setting to set.
    :type name: string
    :param value: Value of the setting to set, being anything but ``nil``.

.. lua:function:: unset(name)

    Unset a setting.

    :param name: Name of the setting to unset.
    :type name: string

.. lua:function:: get(name)

    Get the value of a setting.

    :param name: Name of the setting to get.
    :type name: string
    :return: Value of the setting, or default value for the setting.

Events
------

*This device does not raise events.*

.. _settings module: https://tweaked.cc/module/settings.html
