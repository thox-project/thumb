``computercraft.identification`` -- ComputerCraft computer identification
=========================================================================

.. lua:module:: computercraft.identification

This device can be queried to request the server identification for the
computer.

Available methods
-----------------

.. lua:function:: getComputerID()

    Get the server-wide computer numerical identifier.

    :return: The computer identifier.
    :rtype: number

.. lua:function:: getComputerLabel()

    Get the computer label.

    :return: The computer label, or ``nil`` if no label has been set.
    :rtype: string | nil

.. lua:function:: setComputerLabel(label)

    Set the computer label.

    :param label: The label to set.
    :type label: string | nil

Events
------

*This device does not raise events.*

.. _command peripheral: https://tweaked.cc/peripheral/command.html
