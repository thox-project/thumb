thumb, a unified bootloader for ComputerCraft and OpenComputers
===============================================================

thumb is a unified bootloader for `ComputerCraft: Tweaked`_,
OpenComputers_ and other relevant Lua 5.x environments.

For more information, consult the following references:

* `Documentation <https://thox.madefor.cc/thumb/>`_ (see the ``docs/``
  directory for sources).
* `Issues <https://gitlab.com/thox-project/thumb/-/issues>`_, in case you're
  encountering problems or want to suggest what thumb should add next.

Any question? Any issue? You can either contact me by mail at
thomas@touhey.fr or on the `Computer Mods Discord Server`_ (``@cake``).

.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _OpenComputers: https://ocdoc.cil.li/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
