-- a simple startup script for running thumb in a standardized environment
-- from the startup ComputerCraft: Tweaked environment.
--
-- Copyright (C) 2020-2021, 2024 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thumb project, which is MIT-licensed.
--
-- This file contains a basic compatibility layer which reinterprets all of
-- the basic APIs into a peripheral / component logic, for allowing more
-- advanced OSes to take place.
--
-- For more information about this component, consult the documentation
-- available at <https://thumb.madefor.cc/system/startup.html>.
--
-- Basic variables that should be accessible at all times:
--
--  * `G`: the exported functions to the final system environment,
--    set using the `__index` member of the environment's variable.
--  * `inals`: functions and other utilities defined by the startup script
--    for itself.
--  * `bios`: the isolated functions available from the basic environment
--    from which the startup script is run. These utilities are either used
--    in the rest that this script defines, or disappear for
--    compatibility [1].
--
-- [1] We do not let unsupported utilities filter out to the system in order
--     to avoid systems using machine-specific elements, defeating the purpose
--     of such a script.

local G = {}
local bios = _G
local inals = {}

-- Check if we are in the right environment. Otherwise, there might be a
-- need to change startup script from a rescue environment.
if _HOST == nil or string.sub(_HOST, 1, 14) ~= "ComputerCraft " then
    print("!!! INVALID ENVIRONMENT FOR COMPUTERCRAFT STARTUP:")
    print("      " .. (_HOST or "(none)"))
    print("!!! PLEASE REBOOT IN RESCUE MODE AND FIX THE STARTUP USED.")

    if sleep == nil then
        os.exit()
    end

    while true do
        sleep(60)
    end
end

-- Start to prepare the `inals` (for "internals") table, and prepare
-- the getupvalue function for getting a function upvalue by name.
--
-- getupvalue might not be defined if the `debug` module is not available,
-- or the `debug.getupvalue` function is not defined in our current
-- environment; this might be the case when ComputerCraft doesn't enable
-- the debug module.
inals = {
    version = _VERSION,
    host = _HOST,

    type = type,
    pairs = pairs,
    ipairs = ipairs,
    setmetatable = setmetatable
}

if type(debug) == "table" and type(debug.getupvalue) == "function" then
    local dbg_getupvalue = debug.getupvalue

    inals.getupvalue = function (func, name)
        local i = 1

        while true do
            local e_name, e_val = dbg_getupvalue(func, i)
            if e_name == name then
                return e_val
            elseif e_name == nil then
                return nil
            end
        end
    end
end

-- Reverse some things lost in the native bios (but not everything,
-- unfortunately); for CraftOS bioses, see the following:
-- <https://github.com/SquidDev-CC/CC-Tweaked/blob/b97e950d86e4694409c01e507db34a87da85e452/src/main/resources/data/computercraft/lua/bios.lua>
--
-- TODO: some Lua 5.1 utilities are definitely lost if the
-- option is enabled in ComputerCraft and we have the default BIOS...
-- what to do in that case?
if (_VERSION == "Lua 5.1" and type(bios.os.version) == "function"
    and inals.getupvalue ~= nil) then
    local ver = _VERSION
    local osver = bios.os.version()

    if osver == "CraftOS 1.8" and _CC_DISABLE_LUA51_FEATURES then
        _G.load = inals.getupvalue(load, "nativeload")
        _G.loadstring = inals.getupvalue(load, "nativeloadstring")
        _G.setfenv = inals.getupvalue(load, "nativesetfenv")

        -- TODO: other functions we want to hack.
    end
end

-- Fill the internals table with other useful functions:
--
--  * version: the _VERSION field, copied for it to be accessible at all time.
--  * host: same, but for the _HOST field.
--  * pairs, setmetatable: standard functions to be available at all time
--    during the setup.
--  * table.insert, table.remove: standard functions to be available at
--    all time during the setup, equivalent to table.insert and table.remove.
--  * getupvalue: get an upvalue using a name.
--  * print: print a message on the terminal or standard output, depending
--    on the environment we're on, for the user to see.
--  * panic: exit the startup script by shutting down the computer.
--
-- c_print is currently calibrated for ComputerCraft only. It must be
-- undefined if ComputerCraft is not the current os we're on.
--
-- panic is CCEmuX-aware: if a CCEmuX host is detected, the emulator is
-- closed instead of just shutting down the computer (which would make you
-- obtain a simple black screen).
do
    inals.math = {}

    if math.isinteger ~= nil then
        inals.math.isinteger = math.isinteger
    else
        local floor = math.floor

        function inals.math.isinteger(n)
            return type(n) == "number" and floor(n) == n
        end
    end
end

do
    -- Table function definitions.
    -- We define everything we need here in order not to pollute
    -- other code too much.
    inals.table = {}
    inals.table.insert = table.insert
    inals.table.remove = table.remove

    function inals.table.isSequence(t)
        if type(t) ~= "table" then
            return false
        elseif not inals.math.isinteger(t.n) then
            return false
        end

        return true
    end

    -- Important remark: if `t` is a sequence, it defines the `n` field
    -- and all other fields are integers (numbers).
    function inals.table.sequenceContains(t, e)
        if type(t) ~= "table" then
            return false
        end

        for _, value in inals.ipairs(t) do
            if value == e then
                return true
            end
        end

        return false
    end

    -- This function copies a table with all keys, excluding a given set
    -- of values represented as a sequence. This function works best with
    -- tables without a metatable; only keys and value will be copied,
    -- the source's metatable will not.
    do
        local function copyKeysAndValues(d, s, excludedKeys)
            for key, value in inals.pairs(s) do
                if inals.table.sequenceContains(excludedKeys, key) then
                    -- We do not copy such a key.
                elseif type(value) == "table" then
                    -- We recreate the table and copy recursively.
                    -- This will cause metatables to be lost.

                    d[key] = {}
                    copyKeysAndValues(d[key], s[key], excludedKeys)
                else
                    d[key] = s[key]
                end
            end
        end

        inals.table.copyKeysAndValues = copyKeysAndValues
    end

    -- Interact with sequences.
    function inals.table.addValuesToSequence(t, es)
        for _, value in inals.ipairs(es) do
            inals.table.insert(t, value)
        end
    end

    function inals.table.removeValuesFromSequence(t, es)
        for t_key in #t - 1, 1, -1 do
            local key = t_key

            for _, name in inals.pairs(es) do
                if key == name then
                    inals.table.remove(t, t_key)
                    break
                end
            end
        end
    end
end

do
    local setBackgroundColor = term.setBackgroundColor
    local setTextColor = term.setTextColor
    local clear = term.clear
    local setCursorPos = term.setCursorPos

    local BLACK = 32768
    local WHITE = 1
    local YELLOW = 16
    local RED = 16384
    local last_color = 0

    local function setColors(fore, back)
        if setBackgroundColor ~= nil then
            setBackgroundColor(back)
        end
        if setTextColor ~= nil then
            setTextColor(fore)
        end
    end

    inals.print = function (message, color)
        if color == "red" then
            color = RED
        elseif color == "yellow" then
            color = YELLOW
        else
            color = WHITE
        end

        if color ~= last_color then
            setColors(color, BLACK)
        end

        print(tostring(message))
    end

    clear()
    setCursorPos(1, 1)
end

do
    local startTimer = os.startTimer
    local pullEvent = os.pullEventRaw
    local shutdown = os.shutdown
    local print = inals.print

    if type(ccemux) == "table" and type(ccemux.closeEmu) == "function" then
        shutdown = ccemux.closeEmu
    end

    inals.panic = function ()
        print("The computer will be shut down in 5 seconds.", "red")

        local id = startTimer(5)
        while true do
            local e_type, e_id = pullEvent()
            if e_type == "timer" and e_id == id then
                break
            end
        end

        shutdown()
    end
end

-- Setup the environment by isolating everything that's non
-- standard in a `bios` module, so that we can prepare our own
-- basic APIs with serenity.
--
-- For this we copy what's in _G, as we've only encountered BIOSes which
-- set up their functions in _G for now (using it as an __index on the
-- _ENV metatable).
do
    G._ENV = _ENV
    G._G = G
    G._VERSION = _VERSION

    inals.table.copyKeysAndValues(G, _G, {"_G", "_ENV"})
    inals.G = G
end

-- And now, we set the internals as the __index for the current environment.
-- From now on, any reference to `inals` should be done directly.
inals.setmetatable(_ENV, {__index = inals})

-- Then copy the standard functions of the corresponding version of Lua.
-- See the reference here:
-- <https://thox.madefor.cc/system/startup/startup-api.html#standard-apis>

local supported_versions = {
    ["Lua 5.1"] = true,
    ["Lua 5.2"] = true,
    ["Lua 5.3"] = true
}

if not supported_versions[_VERSION] then
    print("Unsupported Lua version: " .. _VERSION, "red")
    panic()

    -- NOTREACHED
end

do
    local std = {}

    -- Start by listing the standard utilities we want to copy from the
    -- main environment. This will depend on the Lua version only.
    do
        local std = {
            _G = {"assert", "error", "getfenv", "getmetatable", "ipairs",
                "load", "loadstring", "next", "pairs", "pcall", "print",
                "rawequal", "rawget", "rawset", "select", "setfenv",
                "setmetatable", "tonumber", "tostring", "type", "unpack",
                "xpcall"},
            coroutine = {"create", "resume", "running", "status", "wrap",
                "yield"},
            string = {"byte", "char", "dump", "find", "format", "gmatch",
                "gsub", "len", "lower", "match", "rep", "reverse", "sub",
                "upper"},
            table = {"concat", "insert", "maxn", "remove", "sort"},
            math = {"abs", "acos", "asin", "atan", "atan2", "ceil", "cos",
                "cosh", "deg", "exp", "floor", "fmod", "frexp", "huge",
                "ldexp", "log", "log10", "max", "min", "modf", "pi", "pow",
                "rad", "random", "randomseed", "sin", "sinh", "sqrt", "tan",
                "tanh"},
            os = {"clock", "date", "difftime", "time"},
            debug = {"debug", "getfenv", "gethook", "getinfo", "getlocal",
                "getmetatable", "getregistry", "getupvalue", "setfenv",
                "sethook", "setlocal", "setmetatable", "setupvalue",
                "traceback"}
        }

        if _VERSION ~= "Lua 5.1" then
            -- Lua 5.2 and above.

            removeValuesFromSequence(std._G, {"setfenv", "getfenv", "unpack",
                "loadstring"})
            addValuesToSequence(std._G, {"rawlen"})

            removeValuesFromSequence(std.table, {"maxn"})
            addValuesToSequence(std.table, {"pack", "unpack"})

            removeValuesFromSequence(std.math, {"log10"})

            removeValuesFromSequence(std.debug, {"getfenv", "setfenv"})
            addValuesToSequence(std.debug, {"getuservalue", "setuservalue",
                "upvalueid", "upvaluejoin"})

            std.bit32 = {"arshift", "band", "bnot", "bor", "btest",
                "bxor", "extract", "replace", "lrotate", "lshift", "rrotate",
                "rshift"}

            if _VERSION ~= "Lua 5.2" then
                -- Lua 5.3 only.

                addValuesToSequence(std.coroutine, {"isyieldable"})
                addValuesToSequence(std.string, {"pack", "packsize", "unpack"})
                addValuesToSequence(std.table, {"move"})
                removeValuesFromSequence(std.math, {"atan2", "cosh", "frexp",
                    "ldexp", "pow", "sinh"})
                addValuesToSequence(std.math, {"maxinteger", "mininteger",
                    "tointeger", "type", "ult"})

                std.utf8 = {"char", "charpattern", "codes", "codepoint",
                    "len", "offset"}
                std.bit32 = nil
            end
        end
    end

    -- Alright, we have decided on what to pick from the default environment,
    -- let's take it from the bios and add it to our _G now.
    -- Note that this code does not remove the function from the original
    -- `bios` module, just in case.
    do
        for module, functions in pairs(std) do
            if module == "_G" or type(bios[module]) == "table" then
                local src = {}
                local dest = {}

                if module == "_G" then
                    src = bios
                    dest = G
                else
                    src = bios[module]
                    G[module] = {}
                    dest = G[module]
                end

                for _, func_name in pairs(functions) do
                    dest[func_name] = src[func_name]
                end
            end
        end
    end

    -- From here, if necessary, we can check if functions expected to be here
    -- are present, and if it's not the case, try to replace them with
    -- equivalents using other functions.
end

-- Prepare the machine and components system for ComputerCraft.
-- The main properties of each device, available for all, are the following:
--
-- TODO: use numerical ids for ids? That would mean this startup script
-- will have to manage correspondances... which is the case anyway? I don't
-- really know...
--
-- TODO: what about devices that are present on both gateways? Should they
-- be treated as different devices? Be listed as having two buses?
do
    -- Bus identifiers.
    local BUS = {
        CORE = 0,
        SIDE = 1
    }

    local devices = {}
    local event_queue = {}
    local event_by_type = {}
    local keyboard_id
    local device_counter = 0

    local function add_device(bus, addr, device_type, methods)
        local id = device_counter

        device_counter = device_counter + 1
        devices[id] = methods

        data.type = "device_attach"
        data.bus = bus
        data.address = addr
        data.device_type = device_type

        table.insert(event_queue, data)
        return id
    end

    local function remove_device(id)
        table.insert(event_queue, {
            type = "device_detach",
            id = id
        })

        components[id] = nil
    end

    local function add_event_listener(id, data)
        data.id = id

        table.insert(event_queue, data)
    end

    -- Add the buses.
    table.insert(event_queue, {
        type = "bus_attach",
        id = BUS.CORE,
        bus = nil,
        type = "core",
    })
    table.insert(event_queue, {
        type = "bus_attach",
        id = BUS.SIDE,
        bus = 0,
        type = "computercraft.side"
    })

    -- Core devices.
    keyboard_id = add_device(BUS.CORE, "keyboard", "computercraft.keyboard", {})
    add_device(BUS.CORE, "term", "computercraft.terminal", {}) -- TODO
    add_device(BUS.CORE, "fs", "computercraft.harddrive", {}) -- TODO
    add_device(BUS.CORE, "drive", "computercraft.drive", {}) -- TODO
    add_device(BUS.CORE, "clock", "common.clock", {}) -- TODO: in-game clock
    add_device(BUS.CORE, "utc_clock", "common.clock", {}) -- TODO

    do
        local _getComputerID = _G.os.getComputerID
        local _getComputerLabel = _G.os.getComputerLabel
        local _setComputerLabel = _G.os.setComputerLabel

        if _getComputerLabel == nil then
            function _getComputerLabel()
                return nil
            end
        end

        if _setComputerLabel == nil then
            function _setComputerLabel(label)
                -- We cannot set the label here.
            end
        end

        add_device(BUS.CORE, "id", "computercraft.identification", {
            getComputerID = _getComputerID,
            getComputerLabel = _getComputerLabel,
            setComputerLabel = _setComputerLabel
        })
    end

    if _G.settings ~= nil then
        local _rawGetSetting = _G.settings.get
        local _setSetting = _G.settings.set
        local _unsetSetting = _G.settings.unset

        -- Make an intermediary function so we can avoid the default value
        -- being set, in order to avoid having to maintain compatibility with
        -- an unexpected behaviour later.
        local function _getSetting(name)
            return _rawGetSetting(name)
        end

        add_device(BUS.CORE, "settings", "computercraft.settings", {
            set = _setSetting,
            unset = _unsetSetting,
            get = _getSetting
        })
    end

    if _G.redstone ~= nil then
        add_device(BUS.CORE, "redstone", "computercraft.redstone", {}) -- TODO
    end

    if _G.http ~= nil then
        local http = _G.http
        add_device(BUS.CORE, "http", "computercraft.http", {}) -- TODO
    end

    if _G.commands ~= nil then
        local _getCommand = _G.commands.getCommand
        local _setCommand = _G.commands.setCommand
        local _runCommand = _G.commands.runCommand
        add_device(BUS.CORE, "cmd", "computercraft.command", {
            getCommand = _getCommand,
            setCommand = _setCommand,
            runCommand = _runCommand
        })
    end

    if _G.turtle ~= nil then
        add_device(BUS.CORE, "turtle", "computercraft.turtle", {}) -- TODO
    end

    -- Connect peripherals.
    function event_by_type.key(event_name, key_id, is_repeat)
        if is_repeat then
            return
        end

        add_event_listener(keyboard_id, {
            type = "key_down",
            key = key_id
        })
    end

    function event_by_type.key_up(event_name, key_id)
        add_event_listener(keyboard_id, {
            type = "key_up",
            key = key_id
        })
    end

    function event_by_type.paste(event_name, text)
        add_event_listener(keyboard_id, {
            type = "paste",
            value = text
        })
    end

    do
        local mouse_buttons = {"left", "middle", "right"}

        function event_by_type.mouse_click(event_name, button, x, y)
            add_event_listener(keyboard_id, {
                type = "mouse_down",
                button = mouse_buttons[button],
                x = x,
                y = y
            })
        end

        function event_by_type.mouse_scroll(event_name, direction, x, y)
            add_event_listener(keyboard_id, {
                type = "scroll",
                is_up = direction,
                x = x,
                y = y
            })
        end

        function event_by_type.mouse_up(event_name, button, x, y)
            add_event_listener(keyboard_id, {
                type = "mouse_up",
                button = mouse_buttons[button],
                x = x,
                y = y
            })
        end
    end

    -- Setup the event system.
    -- The goal of this system is to associate events with their related
    -- peripheral as listed previously.
    G.pull = function (timeout_sec)
        while true do
            if event_queue then
                local event = table.remove(event_queue, 0)

                return event
            end

            if timeout_sec == 0 then
                return nil
            end

            local timer = bios.os.setTimer(.05)
            while true do
                local event = table.pack(bios.os.pullEventRaw())
                local eventName = event[0]

                if eventName == "timer" then
                    bios.os.cancelTimer(timer)
                    break
                end

                local event_callback = event_by_type[eventName]
                if event_callback ~= nil then
                    event_callback(table.unpack(event))
                end
            end
        end
    end

    G.call = function (id, name, ...)
        local methods = devices[id]

        if methods ~= nil and methods[name] ~= nil then
            return methods[name](...)
        end
    end
end

-- Clean the terminal and show that we're booting up.
-- This is calibrated for ComputerCraft.
do
    local bperipherals = ""

    do
        local key, value

        for key, value in pairs(_G.bus.peripherals) do
            if bperipherals ~= "" then
                bperipherals = bperipherals .. ", "
            end

            bperipherals = bperipherals .. key
        end
    end

    print("thox startup taking place...", "yellow")
    print("", "yellow")
    print("  VERSION = " .. tostring(_VERSION), "yellow")
    print("  HOST = " .. tostring(bios._HOST), "yellow")
    print("  BIOS = " .. tostring(bios.os.version()), "yellow")
    print("", "yellow")
    print("Detected peripherals: " .. bperipherals, "yellow")
    print("", "white")
end

-- Run the system.
setmetatable(_ENV, {__index = G})
