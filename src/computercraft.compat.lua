-- a startup script alias for running thumb in a standardized environment
-- from the startup legacy ComputerCraft environment.
--
-- Copyright (C) 2020-2021 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thumb project, which is MIT-licensed.
--
-- This file is only run on legacy ComputerCraft environments, as newer
-- environments directly run the startup.lua file. We simply implement
-- an alias to run the startup.lua file here as well, as to catch all
-- environments.

shell.run("startup.lua")
